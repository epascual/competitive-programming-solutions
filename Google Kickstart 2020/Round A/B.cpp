#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

const int MN = 52;

int A[MN][MN], AC[MN][MN], SOL[MN][MN][MN*MN], MAX[MN][MN*MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int T; cin >> T;

  for (int caso = 1; caso <= T; caso++){
    memset(A,0,sizeof A);
    memset(AC,0,sizeof AC);
    memset(SOL,0,sizeof SOL);
    memset(MAX,0,sizeof MAX);

    int N, K, P;
    cin >> N >> K >> P;

    for (int i=1; i <= N; i++){
      for (int j=1; j <= K; j++){
        cin >> A[i][j];
        AC[i][j] = A[i][j] + AC[i][j-1];
      }
    }

    int sol=0;
    for (int i=1; i <= N; i++){
      for (int j=0; j <= K; j++){
        if (j > P) break;

        for (int k=0; j+k <= P; k++){
          SOL[i][j][j+k] = AC[i][j] + MAX[i-1][k];
        }
      }

      for (int k=0; k <= P; k++){
        MAX[i][k] = 0;
        for (int j=0; j <= K; j++){
          MAX[i][k] = max(MAX[i][k], SOL[i][j][k]);
        }
      }
    }

    sol = MAX[N][P];

    cout << "Case #" << caso << ": " << sol << "\n";
  }
}
