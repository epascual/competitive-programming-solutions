#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int,int> ii;

const int MN = 3e5+10;
const ll INF = 2e9;

ll X[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int TEST; cin >> TEST;//while (TEST--){
  for (int caso=1; caso <= TEST; caso++){
    cout << "Case #" << caso << ": ";

    ll N, D; cin >> N >> D;

    for (int i=1; i <= N; i++){
      cin >> X[i];
    }

    for (int i=N; i >= 2; i--){
      D -= D % X[i];
    }

    cout << D - D % X[1] << "\n";
  }

  return 0;
}
