#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;

const int MN = 3e5+10;
const ll INF = 1e9;

ld sumat(ll L, ll D){
  ld ret = 0;
  ld pot2 = 1;
  ll comb = 1;
  ld dos = 2;

  for (int i=1; i <= D+1; i++)
    pot2 /= dos;

  for (int i=0; i < L; i++){
    ret += comb*pot2;

    comb *= (D+i+1);
    comb *= (D+1-i+1);
    comb /= (i+1);

    pot2 /= dos;
  }

  return ret;
}

ld DP[350][350];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  cout << fixed << setprecision(9);

  int TEST; cin >> TEST;//while (TEST--){
  for (int caso=1; caso <= TEST; caso++){
    cout << "Case #" << caso << ": ";

    int U,D,L,R,W,H;
    cin >> W >> H >> L >> U >> R >> D;

    /*
    ld ans = 0;
    if (D <= H)
      ans += sumat(D,L);
    if (R <= W)
      ans += sumat(R,U);

    cout << ans << "\n";
    */
    if (W > 300 || W > 300){
        cout << "0\n";
        continue;
    }

    for (int i=1; i <= W; i++){
      for (int j=1; j <= H; j++){
        DP[i][j] = 0;
        if (i >= L && i <= R && j >= U && j <= D){
          continue;
        }
        if (i == 1 && j == 1){
          DP[i][j] = 1;
          continue;
        }
        DP[i][j] = (DP[i-1][j]+DP[i][j-1])/2.0;
        if (i == W){
          DP[i][j] += DP[i][j-1]/2.0;
        }
        if (j == H){
          DP[i][j] += DP[i-1][j]/2.0;
        }
      }
    }

    cout << DP[W][H] << "\n";
  }

  return 0;
}
