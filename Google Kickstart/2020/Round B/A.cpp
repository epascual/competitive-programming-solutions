#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int,int> ii;

const int MN = 3e5+10;
const ll INF = 2e9;

ll A[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int TEST; cin >> TEST;//while (TEST--){
  for (int caso=1; caso <= TEST; caso++){ cout << "Case #" << caso << ": ";
    int N; cin >> N;
    for (int i=1; i <= N; i++){
      cin >> A[i];
    }

    int ans=0;
    for (int i=2; i < N; i++){
      if (A[i] > A[i-1] && A[i] > A[i+1])ans++;
    }
    cout << ans << "\n";
  }

  return 0;
}
