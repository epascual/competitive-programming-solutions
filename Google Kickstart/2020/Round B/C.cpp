#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;

const int MN = 3e5+10;
const ll INF = 1e9;

char cad[MN];
int pareja[MN];

inline void inc(ll &x){
  x++;
  if (x == INF) x = 0;
}
inline void dec(ll &x){
  x--;
  if (x < 0) x = INF-1;
}

ii program(ll x, ll y, char *p){
  if (*p == 0 || *p == ')'){
    return ii(x,y);
  }
  if (isalpha(*p)){
    if (*p == 'N'){
      dec(y);
    }
    if (*p == 'S'){
      inc(y);
    }
    if (*p == 'E'){
      inc(x);
    }
    if (*p == 'W'){
      dec(x);
    }
    return program(x,y,p+1);
  }
  int n = *p - '0';
  ii tmp = program(x,y,p+2);
  tmp.first = (INF + tmp.first - x) % INF;
  tmp.second = (INF + tmp.second - y) % INF;
  for (int i=1; i <= n; i++){
    x += tmp.first;
    y += tmp.second;
    x %= INF;
    y %= INF;
  }
  return program(x,y,cad + (pareja[p-cad+1]) + 1);
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int TEST; cin >> TEST;//while (TEST--){
  for (int caso=1; caso <= TEST; caso++){
    cout << "Case #" << caso << ": ";

    cin >> cad;
    stack<int> s;
    for (int i=1; cad[i]; i++){
      if (cad[i] == '('){
        s.push(i);
      } else if (cad[i] == ')'){
        pareja[s.top()] = i;
        s.pop();
      }
    }

    ii pos = program(0,0,cad);
    cout << pos.first+1 << " " << pos.second+1 << "\n";
  }

  return 0;
}
