#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

const int MN = 1e5+10;

ll A[MN];
ll AC[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int T; cin >> T;

  for (int caso = 1; caso <= T; caso++){
    int N, P; cin >> N >> P;

    for (int i=1; i <= N; i++){
      cin >> A[i];
    }

    sort(A+1,A+1+N);

    int sol = 0;
    for (int i=1; i <= N; i++){
      if (A[i] <= P){
        sol++;
        P -= A[i];
      }
    }

    cout << "Case #" << caso << ": " << sol << "\n";
  }
}
