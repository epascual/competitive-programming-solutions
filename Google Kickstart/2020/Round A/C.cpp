#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

const int MN = 1e5+10;

int A[MN];

bool sepuede(int val, int N, int K){
  for (int i=1; i < N; i++){
    if (A[i+1] - A[i] > val){
      if (K < (A[i+1] - A[i] - 1)/val) return false;
      K -= (A[i+1] - A[i] - 1)/val;
    }
  }
  return true;
}

int solve(){
  int N, K;
  cin >> N >> K;

  for (int i=1; i <= N; i++){
    cin >> A[i];
  }

  int hi = 0;
  for (int i=2; i <= N; i++) hi = max(hi, A[i] - A[i-1]);

  int lo=1, med, sol = hi;


  while (lo <= hi){
    med = (lo + hi)/2;

    if (sepuede(med, N, K)){
      sol = min(sol, med);
      hi = med-1;
    } else {
      lo = med+1;
    }
  }

  return sol;
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int T; cin >> T;

  for (int caso = 1; caso <= T; caso++){

    int sol = solve();

    cout << "Case #" << caso << ": " << sol << "\n";
  }
}
