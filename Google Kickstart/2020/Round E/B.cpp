#include<bits/stdc++.h>
using namespace std;

#define YO(x) if ((x)) cout << "YES\n"; else cout << "NO\n";
typedef long long ll;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 1e6+20;


ll A[MN], B[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ":";

    int N; cin >> N;
    int A, B, C; cin >> A >> B >> C;

    if (N == 1){
      cout << " " << 1 << "\n";
      continue;
    }

    if (N == 2){
      if (A == 2 && B == 2 && C == 2){
        cout << " 1 1\n";
      } else if (C == 1 && A == 1 && B == 2){
        cout << " 2 1\n";
      } else if (C == 1 && A == 2 && B == 1){
        cout << " 1 2\n";
      } else {
        cout << " IMPOSSIBLE\n";
      }
      continue;
    }

    if (A + B - C > N){
      cout << " IMPOSSIBLE\n";
      continue;
    }

    int sobran = N - (A+B-C);

    vector<int> sol;
    bool sobrados = false;
    for (int i=1; i <= A-C; i++){
      sol.push_back(2);
      //cout << " " << 2;
      if (A-C >= 1  && !sobrados){
        for (int i=1; i <= sobran; i++){
          //cout << " " << 1;
          sol.push_back(1);
        }
        sobrados = true;
      }
    }

    for (int i=1; i <= C; i++){
      sol.push_back(3);
      if ((C >= 2 || B-C >= 1) && !sobrados){
        for (int i=1; i <= sobran; i++){
          sol.push_back(1);
        }
        sobrados = true;
      }
    }
    for (int i=1; i <= B-C; i++){
      //cout << " " << 2;
      sol.push_back(2);
    }

    if ((int)sol.size() != N){
      cout << " IMPOSSIBLE";
    } else {
      for (int i=0; i < N; i++){
        cout << " " << sol[i];
      }
    }
    cout << "\n";
  }

  return 0;
}


