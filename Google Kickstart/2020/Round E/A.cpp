#include<bits/stdc++.h>
using namespace std;

#define YO(x) if ((x)) cout << "YES\n"; else cout << "NO\n";
typedef long long ll;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 1e6+20;


ll A[MN], B[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ": ";

    int N; cin >> N;
    for (int i=1; i <= N; i++) cin >> A[i];

    for (int i=2; i <= N; i++) B[i] = A[i] - A[i-1];

    int sol=0;
    for (int i=2; i <= N; ){
      int j;
      for (j=i; j <= N && B[j] == B[i]; j++){
        sol = max(sol, j-i+1);
      }
      i=j;
    }

    cout << sol+1 << "\n";
  }

  return 0;
}


