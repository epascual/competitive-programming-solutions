#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const int MN = 3e5+10;

int a[MN];
int ld[MN], rd[MN]; //l-difference, r-difference
int lc[MN];         //l-count
int rc[MN];         //r-count

void solve(){
  int N; cin >> N;

  for (int i=1; i <= N; i++){
    cin >> a[i];
  }

  int sol = 2;


  for (int i=2; i <= N; i++){
    ld[i] = a[i] - a[i-1];
    if (i > 2 && ld[i] == ld[i-1]){
      lc[i] = lc[i-1] + 1;
      sol = max(sol, lc[i]+1);
    }
    else{
      lc[i] = 1;
    }
  }
  if (sol < N) sol++;

  for (int i = N-1; i >= 1; i--){
    rd[i] = a[i+1] - a[i];
    if (i < N-1 && rd[i] == rd[i+1]){
      rc[i] = rc[i+1] + 1;
    }
    else{
      rc[i] = 1;
    }
  }

  for (int i=2; i < N; i++){
    if ((a[i-1] + a[i+1]) % 2 == 0){
      int d = (a[i-1] + a[i+1])/2;
      int tmp = 1;
      if (ld[i-1] == d-a[i-1]) tmp += lc[i-1]+1;
      else tmp += 1;
      if (rd[i+1] == a[i+1]-d) tmp += rc[i+1]+1;
      else tmp += 1;

      sol = max(tmp, sol);
    }
  }

  //sol = max(sol, lc[N-1]+2);
  //sol = max(sol, rc[2]+2);

  cout << sol << "\n";
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests;

  for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";
    solve();
  }

  return 0;
}
