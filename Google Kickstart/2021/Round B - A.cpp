#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const int MN = 1e5+10;

string cad;

void solve(){
  int N; cin >> N;
  cin >> cad;

  int a = 1;
  cout << "1";
  for (int i=1; i < N; i++){
    if (cad[i] > cad[i-1]){
      a++;
    } else {
      a = 1;
    }
    cout << " " << a;
  }
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests;

  for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";

    solve();

    cout << "\n";
  }

  return 0;
}
