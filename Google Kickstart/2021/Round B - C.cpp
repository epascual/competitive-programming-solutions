#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const int MN = 1e9+10;

//ll A[MN];
//ll B[MN];

bool criba[MN];
struct A{
  ll num;
  ll idx;
  ll sol;

  bool operator< (const A &o) const{
    return num < o.num;
  }
} a[110];

ll sol[110];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests;

  for (int i=1; i <= Tests; i++){
    cin >> a[i].num;
    a[i].idx = i;
  }

  sort(a+1, a+1+Tests);

  ll k=1;

  for (ll i=4; i < MN; i += 2) criba[i] = 1;
  ll ant = 2;
  for (ll i=3; i < MN; i += 2) if (!criba[i]){
    ll prod = ant*i;
//    if (prod <= a[k].num){
//      a[k].sol = prod;
//    } else {
//      while (k <= Tests && prod > a[k].num){
//        a[k].sol = a[k-1].sol;
//        k++;
//      }
//    }
    while (k <= Tests && prod > a[k].num){
      k++;
      a[k].sol = a[k-1].sol;
    }
    if (k > Tests) break;
    a[k].sol = prod;

    ant = i;
    for (ll j=i*i; j < MN; j += 2*i){
      criba[j] = 1;
    }
  }

  for (int i=1; i <= Tests; i++){
    sol[a[i].idx] = a[i].sol;
  }

  for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";

    cout << sol[Test] << "\n";
  }

  return 0;
}
