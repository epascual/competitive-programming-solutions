#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;
typedef pair<ll,ii> iii;
typedef vector<int> vi;
const ll INF = 1e18;

const ll MN = 2021;
const ll mod = 1e9+7;
const ld EPS = 1e-8;

#define X first
#define Y second

#define YN(x) if ((x)) cout << "YES\n"; else cout << "NO\n";

int a[MN][MN];

int l[MN][MN], r[MN][MN], d[MN][MN], u[MN][MN];

int f(int x, int y){
  return max(0,min(x,y)-1);
}

int main() {
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test=1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";

    int R, C; cin >> R >> C;

    for (int i=1; i <= R; i++){
      l[i][0] = l[i][C+1] = 0;
      r[i][0] = r[i][C+1] = 0;
      for (int j=1; j <= C; j++){
        cin >> a[i][j];
        l[i][j] = r[i][j] = u[i][j] = d[i][j] = 0;
      }
    }
    for (int j=1; j <= C; j++){
      u[0][j] = u[R+1][j] = 0;
      d[0][j] = d[R+1][j] = 0;
    }

    for (int i=1; i <= R; i++){
      for (int j=1; j <= C; j++) if (a[i][j]){
        l[i][j] = l[i][j-1] + 1;
      }
      for (int j=C; j >= 1; j--) if (a[i][j]){
        r[i][j] = r[i][j+1] + 1;
      }
    }

    for (int j=1; j <= C; j++){
      for (int i=1; i <= R; i++) if (a[i][j]){
        u[i][j] = u[i-1][j] + 1;
      }
      for (int i=R; i >= 1; i--) if (a[i][j]){
        d[i][j] = d[i+1][j] + 1;
      }
    }

    ll res = 0;
    for (int i=1; i <= R; i++){
      for (int j=1; j <= C; j++){
          res += f(u[i][j], l[i][j]/2),
          res += f(u[i][j], r[i][j]/2);

          res += f(d[i][j], l[i][j]/2),
          res += f(d[i][j], r[i][j]/2);

          res += f(l[i][j], u[i][j]/2),
          res += f(l[i][j], d[i][j]/2);

          res += f(r[i][j], u[i][j]/2),
          res += f(r[i][j], d[i][j]/2);
      }
    }

    cout << res << "\n";
  }

  return 0;
}


