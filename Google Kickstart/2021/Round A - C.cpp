#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;
typedef pair<ll,ii> iii;
typedef vector<int> vi;
const ll INF = 1e18;

const ll MN = 2021;
const ll mod = 1e9+7;
const ld EPS = 1e-8;

//#define X first
//#define Y second

#define YN(x) if ((x)) cout << "YES\n"; else cout << "NO\n";

int a[MN][MN];
bool mark[MN][MN];

typedef pair<int,ii> tri;
#define val first
#define X second.first
#define Y second.second

int mx[] = {-1,1,0,0}, my[] = {0,0,-1,1};

inline bool valid(int x, int y, int R, int C){
  return x <= R && x >= 1 && y <= C && y >= 1;
}

int main() {
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test=1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";

    int R, C; cin >> R >> C;

    set<tri, greater<tri> > pq;
    for (int i=1; i <= R; i++){
      for (int j=1; j <= C; j++){
        cin >> a[i][j];
        pq.insert(tri(a[i][j],ii(i,j)));

        mark[i][j] = 0;
      }
    }

    ll res = 0;
    while (!pq.empty()){
      tri elem = (*pq.begin()); pq.erase(pq.begin());

      int v = elem.val, i = elem.X, j = elem.Y;
      mark[i][j] = 1;

      for (int k=0; k < 4; k++){
        int ni = i + mx[k], nj = j + my[k];
        if (valid(ni,nj,R,C) && !mark[ni][nj]){
          res += max(0, (v-1) - a[ni][nj]);

          pq.erase(pq.find(tri(a[ni][nj],ii(ni,nj))));
          a[ni][nj] = max(a[ni][nj], v-1);
          pq.insert(tri(a[ni][nj],ii(ni,nj)));
        }
      }
    }

    cout << res << "\n";
  }

  return 0;
}


