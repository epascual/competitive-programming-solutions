#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;
typedef pair<ll,ii> iii;
typedef vector<int> vi;
const ll INF = 1e18;

const ll MN = 2e5+10;
const ll mod = 1e9+7;
const ld EPS = 1e-8;

#define X first
#define Y second

#define YN(x) if ((x)) cout << "YES\n"; else cout << "NO\n";

char cad[MN];

int main() {
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test=1; Test <= Tests; Test++){
      cout << "Case #" << Test << ": ";
    int N, K; cin >> N >> K;

    cin >> cad;

    int cant = 0;
    for (int i=0; i < N-i-1; i++){
      if (cad[i] != cad[N-i-1]) cant++;
    }

    cout << abs(cant - K) << "\n";
  }

  return 0;
}


