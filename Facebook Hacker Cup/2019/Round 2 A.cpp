#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 2e6+20;

#define YN(x) if ((x)) cout << "Y\n"; else cout << "N\n";

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ": ";

    int N, M, K; cin >> N >> M >> K;

    int a, b; cin >> a >> b;
    int c1, r1, c2, r2; cin >> c1 >> r1;

    if (K == 1){
      cout << "N\n";
    } else {
      cin >> c2 >> r2;

      bool sepuede = (a+b)%2 == (c1+r1)%2 && (a+b)%2 == (c2+r2)%2;

      YN(sepuede);
    }
  }

  return 0;
}


