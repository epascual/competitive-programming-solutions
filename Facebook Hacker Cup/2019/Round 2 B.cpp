/*
 * #disjointsets
 * #sumdp
 */

#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 2e6+20;

#define YN(x) if ((x)) cout << "Y\n"; else cout << "N\n";

vector<int> pset;
//vector<int> cant;
void init(int n){
  pset.resize(n+1);
  //cant.resize(n+1);
  for (int i=1; i<=n; i++) pset[i] = i;
  //for (int i=1; i<=n; i++) cant[i] = 1;
}
int findSet(int pos){
  return (pset[pos] == pos) ? pos : (pset[pos] = findSet(pset[pos]));
}
bool isSameSet(int i, int j){return findSet(i) == findSet(j);}
void unionSet(int i, int j){
  pset[findSet(i)] = findSet(j);
}



int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ": ";

    int N, M; cin >> N >> M;

    init(N+1);

    for (int i=1; i <= M; i++){
      int x, y; cin >> x >> y;

      for (int l1 = x, l2 = y; l1 <= l2; l1++, l2--){
        unionSet(l1, l2);
      }
    }

    map<ll,ll> m;
    for (int i=1; i <= N; i++){
      m[findSet(i)]++;
    }

    vector< ii > elems;
    for (auto x : m){
      elems.push_back(ii(x.second, x.first));
    }

    vector<int> dp(N+1, 0);
    vector<int> pred(N+1, 0);
    dp[0] = 1;

    for (ii x : elems){
      ll val = x.first;
      ll rep = x.second;

      for (int i=N; i >= val; i--){
        if (!dp[i] && dp[i-val]){
          dp[i] = 1;
          pred[i] = rep;
        }
      }
    }

    int mindif = N+1, difpos=-1;
    for (int i=1; i <= N; i++){
      if (dp[i] && abs(i-(N-i)) < mindif){
        mindif = abs(i-(N-i));
        difpos = i;
      }
    }

    map<int,int> color;
    while (difpos){
      color[pred[difpos]] = 1;
      difpos -= m[pred[difpos]];
    }

    for (int i=1; i <= N; i++){
      if (color.find(findSet(i)) != color.end()){
        cout << "1";
      } else {
        cout << "0";
      }
    }

    cout << "\n";
  }

  return 0;
}


