#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 2020202;

#define YN(x) if ((x)) cout << "Y\n"; else cout << "N\n";

int N, K;

ll S[MN], X[MN], Y[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ": ";

    cin >> N >> K;

    ll AS, BS, CS, DS;
    ll AX, BX, CX, DX;
    ll AY, BY, CY, DY;

    for (int i=1; i <= K; i++){
      cin >> S[i];
    }
    cin >> AS >> BS >> CS >> DS;

    for (int i=1; i <= K; i++){
      cin >> X[i];
    }
    cin >> AX >> BX >> CX >> DX;

    for (int i=1; i <= K; i++){
      cin >> Y[i];
    }
    cin >> AY >> BY >> CY >> DY;

    for (int i=K+1; i <= N; i++){
      S[i] = ((AS*S[i-2] + BS*S[i-1] + CS) % DS);
      X[i] = ((AX*X[i-2] + BX*X[i-1] + CX) % DX);
      Y[i] = ((AY*Y[i-2] + BY*Y[i-1] + CY) % DY);
    }

    ll minO=0, maxO=0;
    ll minI=0, maxI=0;
    for (int i=1; i <= N; i++){
      if (S[i] < X[i]){
        minI += X[i]-S[i];
        maxI += X[i]+Y[i]-S[i];
      }
      else if (S[i] > X[i] + Y[i]){
        minO += S[i] - (X[i]+Y[i]);
        maxO += S[i] - X[i];
      }
      else {
        maxI += X[i]+Y[i]-S[i];
        maxO += S[i] - X[i];
      }
    }

    if (minI >= minO && minI <= maxO) cout << minI << "\n";
    else if (minO >= minI && minO <= maxI) cout << minO << "\n";
    else cout << -1 << "\n";
  }

  return 0;
}


