/*
 * #disjointsets
 * #segtree #segtreesum #segtreemin 
 */
 #include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 2e6+20;

vector<int> pset;
void init(int n){ pset.resize(n+1); for (int i=1; i<=n; i++) pset[i] = i;}
int findSet(int pos){return (pset[pos] == pos) ? pos : (pset[pos] = findSet(pset[pos]));}
bool isSameSet(int i, int j){return findSet(i) == findSet(j);}
void unionSet(int i, int j){pset[findSet(i)] = findSet(j);}

#define left(n) (2*(n))
#define right(n) (2*(n)+1)

struct node {
    ll v;     //valor del nodo
    bool act;  //hay que actualizar?
    ll vact;  //con que hay que actualizar
    node(){act = 0;}
};

struct SegTree {
    node ST[4*MN]; ///se puede optimizar

    void init(int n){
        memset(ST, 0, sizeof ST);
    }

    void update_lazily(int n, int s, int e, ll val){
        if (s+1 == e)
            ST[n].v = val;
        else {
          ST[n].act = true;
          ST[n].vact = val;
          ST[n].v = val * (e-s);
        }
    }

    void update(int n, int s, int e, int a, int b, ll val){
        if (e <= a || s >= b) return;
        if (s >= a && e <= b){
            update_lazily(n, s, e, val);
        } else {
            if (ST[n].act){
                ST[n].act = false;
                update_lazily(left(n),s,(s+e)/2,ST[n].vact);
                update_lazily(right(n),(s+e)/2,e,ST[n].vact);
            }
            update(left(n),s,(s+e)/2,a,b, val);
            update(right(n),(s+e)/2,e,a,b, val);
            ST[n].v = ST[left(n)].v + ST[right(n)].v;
    }   }

    ll query(int n, int s, int e, int a, int b){
        if (e <= a || s >= b) return 0;
        else if (s >= a && e <= b) {
            return ST[n].v;
        }
        if (ST[n].act){
            ST[n].act = false;
            update_lazily(left(n),s,(s+e)/2,ST[n].vact);
            update_lazily(right(n),(s+e)/2,e,ST[n].vact);
        }
        ll l = query(left(n),s,(s+e)/2,a,b);
        ll r = query(right(n),(s+e)/2,e,a,b);
        return (l+r);
    }
} st;

struct SegTreeM {
    node ST[4*MN]; ///se puede optimizar

    void init(int n){
        memset(ST, 0, sizeof ST);
    }

    void update_lazily(int n, int s, int e, ll val){
        if (s+1 == e)
            ST[n].v = val;
        else {
            ST[n].act = true;
            ST[n].vact = val;
            ST[n].v = val;
        }
    }

    void update(int n, int s, int e, int a, int b, ll val){
        if (e <= a || s >= b) return;
        if (s >= a && e <= b){
            update_lazily(n, s, e, val);
        } else {
            if (ST[n].act){
                ST[n].act = false;
                update_lazily(left(n),s,(s+e)/2,ST[n].vact);
                update_lazily(right(n),(s+e)/2,e,ST[n].vact);
            }
            update(left(n),s,(s+e)/2,a,b, val);
            update(right(n),(s+e)/2,e,a,b, val);
            ST[n].v = max(ST[left(n)].v,ST[right(n)].v);
    }   }

    ll query(int n, int s, int e, int a, int b){
        if (e <= a || s >= b) return 0;
        else if (s >= a && e <= b) {
            return ST[n].v;
        }
        if (ST[n].act){
            ST[n].act = false;
            update_lazily(left(n),s,(s+e)/2,ST[n].vact);
            update_lazily(right(n),(s+e)/2,e,ST[n].vact);
        }
        ll l = query(left(n),s,(s+e)/2,a,b);
        ll r = query(right(n),(s+e)/2,e,a,b);
        return max(l,r);
    }
} stm;

map<ii,ll> m;
map<ll,ll> _l;
map<ll,ll> _w;
map<ll,ll> _h;

bool matches(int x, int y){
//  int lx = _l[x];
//  int ly = _l[y];
//  int wx = _w[x];
//  int wy = _w[y];
  return (_l[x] >= _l[y] && _l[x] <= _w[y])
      || (_l[y] >= _l[x] && _l[y] <= _w[x]);
}

ll P[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ": ";

    int N, K;
    cin >> N >> K;

    vector<ll> L(N+1, 0);
    for (int i=1; i <= K; i++) cin >> L[i];

    ll AL, BL, CL, DL;
    cin >> AL >> BL >> CL >> DL;

    vector<ll> W(N+1, 0);
    for (int i=1; i <= K; i++) cin >> W[i];

    ll AW, BW, CW, DW;
    cin >> AW >> BW >> CW >> DW;

    vector<ll> H(N+1, 0);
    for (int i=1; i <= K; i++) cin >> H[i];

    ll AH, BH, CH, DH;
    cin >> AH >> BH >> CH >> DH;

    for (int i=K+1; i <= N; i++){
      L[i] = ((AL*L[i-2] + BL*L[i-1] + CL) % DL)+1LL;
      W[i] = ((AW*W[i-2] + BW*W[i-1] + CW) % DW)+1LL;
      H[i] = ((AH*H[i-2] + BH*H[i-1] + CH) % DH)+1LL;
    }

    init(N);
    m.clear();
    _l.clear();
    _w.clear();
    _h.clear();

    ll ans=2LL*(W[1]+H[1]);
    //vector<ll> P(N+1,0);
    for (int i=1; i <= N; i++) P[i] = 0;

    vector<ll> ptos;
    for (int i=1; i <= N; i++){
      _l[i] = L[i];
      _w[i] = L[i]+W[i];
      _h[i] = H[i];
      ptos.push_back(L[i]);
      ptos.push_back(L[i]+W[i]);
    }
    sort(ptos.begin(), ptos.end());
    map<int,int> f; int F=0;
    for (auto x : ptos){
      if (f.find(x) == f.end())
        f[x] = F++;
    }

    vector<bool> inc(F+1, 0);

    st.init(F+1);
    stm.init(F+1);

    P[1] = ans;
    m[ii(L[1], L[1]+W[1])] = 1;
    st.update(1,0,F, f[L[1]], f[L[1]]+1, H[1]);
    st.update(1,0,F, f[L[1]+W[1]], f[L[1]+W[1]]+1, H[1]);
    stm.update(1,0,F, f[L[1]], f[L[1]+W[1]]+1, H[1]);
    inc[f[L[1]]] = 1;

    for (int i=2; i <= N; i++){
      P[i] = P[i-1];

      auto l = m.lower_bound(ii(L[i],L[i]));
      auto r = m.lower_bound(ii(L[i]+W[i],L[i]+W[i]));

      if (l != m.begin()) l--;
      //if (r != m.begin()) r--;

      vector<ii> keys;

      for (auto it = l; it != r; it++){
        int idx = it->second;
        int pos = findSet(idx);

        if (matches(i, pos)){
          keys.push_back(it->first);
          ll perim = 2LL*(_w[pos] - _l[pos]);
          perim %= mod;
          P[i] = (P[i] + mod - perim)%mod;
        }
      }
      for (auto it = l; it != r; it++){
        int idx = it->second;
        int pos = findSet(idx);

        if (matches(i, pos)){
          unionSet(pos, i);
          int pos2 = findSet(i);
          _l[pos2] = min(_l[pos], _l[pos2]);
          _w[pos2] = max(_w[pos], _w[pos2]);
          _h[pos2] = max(_h[pos], _h[pos2]);
        }
      }

      for (ii x : keys) { m.erase(m.find(x));}

      int pos3 = findSet(i);
      m[ii(_l[pos3],_w[pos3])] = pos3;
      P[i] += 2LL*(_w[pos3] - _l[pos3]);
      P[i] %= mod;

      ll hs = st.query(1, 0, F, f[L[i]], f[L[i]+W[i]]+1);

      ll h1 = (H[i] - (stm.query(1, 0, F, f[L[i]], f[L[i]]+1)));
      if (inc[f[L[i]]]){
        h1 += st.query(1, 0, F, f[L[i]], f[L[i]]+1);
      }

      ll h2 = (H[i] - (stm.query(1, 0, F, f[L[i]+W[i]], f[L[i]+W[i]]+1)));
      if (!inc[f[L[i]+W[i]]]){
        h2 += st.query(1, 0, F, f[L[i]+W[i]], f[L[i]+W[i]]+1);
      }

      inc[f[L[i]]] = 1;
      inc[f[L[i]+W[i]]] = 0;

      P[i] += (mod + h1 + h2 - hs);
      P[i] %= mod;

      st.update(1, 0, F, f[L[i]]+1, f[L[i]+W[i]], 0);
      st.update(1, 0, F, f[L[i]], f[L[i]]+1, h1);
      st.update(1, 0, F, f[L[i]+W[i]], f[L[i]+W[i]]+1, h2);

      stm.update(1, 0, F, f[L[i]], f[L[i]+W[i]]+1, H[i]);

      ans = (ans * P[i])%mod;
    }

    cout << ans << "\n";
  }

  return 0;
}


