#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;
const ll mod = 1e9+7;
const ll MN = 2020202;

#define YN(x) if ((x)) cout << "Y\n"; else cout << "N\n";

vector<vector<ld>> tmp;

#define C2(x) ((ld)((x)*(x-1))/2.0)

void rec(int N, ld P){
  if (tmp[N].size() != 0) return;

  tmp[N] = vector<ld> (N,1);
  vector<ld> sum(N,0);

  if (N == 2) return;

//  ld *x = &tmp[N][0];
//  ld *y = &sum[0];

  rec(N-1, P);

  for (int i=0; i < N; i++){

//    for (int j=i+1; j < N; j++){
//      sum[j] += P*tmp[N-1][j-1];
//      sum[i] += (1.0-P)*tmp[N-1][i];
//    }
    if (i){
      sum[i] += P*(i)*tmp[N-1][i-1];
      sum[i] += C2(i)*tmp[N-1][i-1];
    }
    if (i < N-1){
      sum[i] += (1.0-P)*(N-i-1)*tmp[N-1][i];
      sum[i] += C2(N-i-1)*tmp[N-1][i];
    }
    if (i && i < N-1){
      sum[i] += P*(i)*(N-i-1)*tmp[N-1][i-1];
      sum[i] += (1.0-P)*(i)*(N-i-1)*tmp[N-1][i];
    }
  }

  for (int i=0; i < N; i++){
    tmp[N][i] += sum[i]*2.0/((ld)(N-1)*(N));
  }


}

void solve(int N, ld P){
  tmp = vector<vector<ld>> (N+1, vector<ld>(0));

  rec(N, P);
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  cout << fixed << setprecision(8);

  int Test; cin >> Test; for (int TestN=1; TestN <= Test; TestN++){
    cout << "Case #" << TestN << ":\n";

    ll N; ld P;
    cin >> N >> P;

    solve(N, P);

    for (int i=0; i < N; i++){
      cout << tmp[N][i] << "\n";
    }
  }

  return 0;
}


