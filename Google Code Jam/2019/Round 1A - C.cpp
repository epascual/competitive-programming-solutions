/**
  * #Trie
  */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;
const int MN = 1e5+10;

struct node {
  map<char,node> ady;
};

struct trie {
  node root;

  void insert(char *word){
    insert(root.ady[word[0]], word);
  }

  void insert(node &nod, char *word){
    if (!word[0]) return;
    word++;
    insert(nod.ady[word[0]], word);
  }
};

int dfs(node &tmp, int &sol, int ant = -1){
  if (ant == 0) return 1;

  int ret = 0;
  for (auto ady : tmp.ady){
      ret += dfs(ady.second, sol, ady.first);
  }

  if (ret >= 2){
    sol += 2;
    ret -= 2;
  }
  return ret;
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";
    int N; cin >> N;

    trie t;

    for (int i=1; i <= N; i++){
      string cad; cin >> cad;
      reverse(cad.begin(), cad.end());
      t.insert(&cad[0]);
    }

    int sol=0;
    for (auto &tmp : t.root.ady)
      dfs(tmp.second, sol);

    cout << sol << "\n";
  }

  return 0;
}
