#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
const int MN = 1e5+10;

char cad[MN][110];
int len[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  vector<int> Ms({18,17,13,11,7,5});
  

  int Tests; cin >> Tests;
  int N, M; cin >> N >> M;

  for (int Test = 1; Test <= Tests; Test++){
    vector<int> day(6,0);
    for (int j=0; j < 6; j++){
      int x = Ms[j];
      for (int i=0; i < 18; i++){
        if (i) cout << " ";
        cout << x;
      }
      cout << endl;

      ll tmp=0;
      for (int i=0; i < 18; i++){
        cin >> tmp;
        day[j] += tmp;
      }
      day[j] %= x;
    }

    for (int i=1; i <= M; i++){
      bool isanswer = true;
      for (int j=0; j < 6; j++){
        if (day[j] != i%Ms[j]) {
          isanswer = false;
          break;
        }
      }
      if (isanswer){
        cout << i << endl;
        int tmp; cin >> tmp;
        if (tmp == -1) return 0;
      }
    }
  }

  return 0;
}
