#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll,ll> ii;
const int MN = 1e5+10;

ll sobran[MN], M;

ll possible(ll sum, ll s, vector<ii> &prim){
  ll s1 = sum - s;

  ll quit = 0;

  for (int i=1; i <= M; i++){
    sobran[i] = prim[i].second;
    while (s1 % prim[i].first == 0){
      s1 /= prim[i].first;
      sobran[i]--;
      quit += prim[i].first;
    }
    if (sobran[i] < 0) return 0;
  }
  
  if (s1 > 1) return 0;

  if (quit != s) return 0;

  ll s2=0;
  for (int i=1; i <= M; i++){
    s2 += sobran[i] * prim[i].first;
  }

  if (s2 != sum-s) return 0;

  return sum - s;
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests;

  for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";
    cin >> M;
    vector<ii> prim (M+1);

    ll sum = 0;
    for (int i=1; i <= M; i++){
      cin >> prim[i].first >> prim[i].second;
      sum += prim[i].first * prim[i].second;
    }

    ll ret = 0;
    for (ll s = 1; s < min(sum,60LL*499); s++){
      ll tmp = possible(sum,s,prim);
      if (tmp){
        ret = tmp;
        break;
      }
      //ret = max(ret, tmp);
    }
    cout << ret << "\n";
  }

  return 0;
}
