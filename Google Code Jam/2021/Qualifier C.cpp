#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;
typedef pair<ll,ii> iii;
typedef vector<int> vi;
const ll INF = 1e18;

const ll MN = 1e5+10;
const ll mod = 1e9+7;
const ld EPS = 1e-8;

int a[MN];

int main() {
  #ifdef PRIVADO
  freopen("a.in","r",stdin); //freopen("a.out","w",stdout);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test=1; Test <= Tests; Test++){
    cout << "Case #" << Test << ":";

    ll N, C; cin >> N >> C;

    C -= N-1;

    if (C < 0 || C > (N-1)*(N)/2){
      cout << " IMPOSSIBLE\n";
      continue;
    }

    for (int i=1; i <= N; i++) a[i] = i;

    vector<ii> swaps;

    for (ll i=1; i < N; i++){
      ll val = min(C, N-i);
      C -= val;
      swaps.push_back(ii(i, i+val+1));
    }

    reverse(swaps.begin(), swaps.end());
    for (auto e : swaps){
      reverse(a+e.first, a+e.second);
    }

    for (int i=1; i <= N; i++){
      cout << " " << a[i];
    }
    cout << "\n";
  }

  return 0;
}


