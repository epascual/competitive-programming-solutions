#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;
const int MN = 1e4+10;

char a[MN][1010];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";
    int N; cin >> N;

    for (int i=1; i <= N; i++){
      cin >> a[i];
    }

    int sol = 0;
    for (int i=2; i <= N; i++){
      int l1 = strlen(a[i-1]);
      int l2 = strlen(a[i]);

      if (l2 > l1) continue;

      if (l2 == l1){
        if (strcmp(a[i-1],a[i]) >= 0){
          sol++;
          strcat(a[i],"0");
        }
        continue;
      }

      if (strncmp(a[i-1],a[i],l2) == 0){
        strcat(a[i],a[i-1]+l2);
        sol += l1-l2;

        int ac = 1;
        for (int j=l1-1; ac && j >= l2; j--){
          if (a[i][j] == '9'){
            a[i][j] = '0';
          } else {
            a[i][j]++;
            ac = 0;
          }
        }

        if (ac == 1){
          strcat(a[i],"0");
          sol++;
        }

      } else if (strncmp(a[i-1],a[i],l2) < 0) {
        for (int j=0; j < l1-l2; j++){
          strcat(a[i],"0");
          sol++;
        }
      } else {
        for (int j=0; j < l1-l2+1; j++){
          strcat(a[i],"0");
          sol++;
        }
      }
    }

    cout << sol << "\n";
  }

  return 0;
}
