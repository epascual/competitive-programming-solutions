#include <bits/stdc++.h>
using namespace std;

const int MN = 1e5+10;

int a[MN];

int main()
{
    #ifdef PRIVADO
    freopen("a.in","r",stdin);
    #else
    cin.tie(0); ios_base::sync_with_stdio(0);
    #endif // PRIVADO

    int Tests; cin >> Tests; for (int Test = 1; Test <= Tests; Test++){
        cout << "Case #" << Test << ": ";

        int N; cin >> N;

        for (int i=1; i <= N; i++){
            cin >> a[i];
        }

        int res = 0;
        for (int i=1; i <= N-1; i++){
            int mij=i;
            for (int j=i+1; j <= N; j++){
                if (a[j] < a[mij]){
                    mij = j;
                }
            }
            res += (mij-i)+1;
            reverse(a+i, a+mij+1);
        }

        cout << res << "\n";
    }

    return 0;
}
