#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;

const int MN = 1e4 + 10;
const ll INF = 1e9;

struct QU {
  string A;
  string cad;

  bool operator< (const QU & o)const {
    return A < o.A;
  }
} qu[MN];

bool founded[12];
char res[12] = {};
set<char> posibles[12];
int used[256];
int cant[256];
int notzero[256];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int TEST; cin >> TEST; for (int caso=1; caso <= TEST; caso++){
    cout << "Case #" << caso << ": ";

    ll N, U; N = 1e4; cin >> U;
    for (int i=1; i <= N; i++){
      cin >> qu[i].A >> qu[i].cad;

      for (int j=0; qu[i].cad[j]; j++)
        used[(int)qu[i].cad[j]]=1;
      notzero[(int)qu[i].cad[0]] = 1;
      cant[(int)qu[i].cad[0]]++;
    }

    for (int i = 'A'; i <= 'Z'; i++){
      if (used[i] && !notzero[i]){
        res[0] = i;
      }
    }

    cant[res[0]] = 0;
    for (int i=1; i <= 9; i++){
      ll tmp=0, val;
      for (int j='A'; j <= 'Z'; j++){
        if (cant[j] > tmp){
          tmp = cant[j];
          val = j;
        }
      }
      res[i] = val;
      cant[val] = 0;
    }

    for (int i=0; i < 10; i++) cout << res[i];
    cout << "\n";

    memset(used,0,sizeof used);
    memset(notzero,0,sizeof notzero);
    memset(cant,0,sizeof cant);
    for (int i=0 ; i < 10; i++){
      founded[i] = false;
      posibles[i].clear();
    }
  }

  return 0;
}
