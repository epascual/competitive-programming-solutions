#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
const int MN = 1e5+10;

char cad[MN][110];
int len[MN];

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ": ";
    int N; cin >> N;

    for (int i=1; i <= N; i++){
      cin >> cad[i];
      len[i] = strlen(cad[i]);
    }

    int lenPF = 0, posPF = 0;
    int lenSF = 0, posSF = 0;
    for (int i=1; i <= N; i++){
      int j;
      for (j=0;cad[i][j] != '*';j++);
      if (j > lenPF) lenPF = j, posPF = i;

      for (j=len[i]-1; cad[i][j] != '*'; j--);
      j = len[i]-1-j;
      if (j > lenSF) lenSF = j, posSF = i;
    }

    bool sepuede = true;
    for (int i=1; sepuede && i <= N; i++){
      for (int j=0; cad[i][j] != '*'; j++){
        if (cad[i][j] != cad[posPF][j]){
          sepuede = false;
          break;
        }
      }
      for (int j=0; cad[i][len[i]-1-j] != '*'; j++){
        if (cad[i][len[i]-1-j] != cad[posSF][len[posSF]-1-j]){
          sepuede = false;
          break;
        }
      }
    }

    if (!sepuede){
      cout << "*\n";
    } else {
      string ret = "";
      for (int j=0; j < lenPF; j++){
        ret += cad[posPF][j];
      }

      string med="";
      for (int i=1; i <= N; i++){
        string tmp="";
        int f=-1,l;
        for (int j=0; cad[i][j]; j++){
          if (cad[i][j] == '*'){
            if (f == -1) f = j;
            l = j;
          }
        }
        for (int j=f; j <= l; j++){
          if (cad[i][j] != '*'){
            tmp += cad[i][j];
          }
        }
        med += tmp;
      }

      string tmp="";
      for (int j=0; j < lenSF; j++){
        tmp += cad[posSF][len[posSF]-1-j];
      }
      reverse(tmp.begin(),tmp.end());

      cout << ret+med+tmp << "\n";
    }
  }

  return 0;
}
