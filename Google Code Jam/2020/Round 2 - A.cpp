#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
const int MN = 1e5+10;

ll L, R;

ll bb1(ll dif){
  ll res;
  ll lo = 0, hi = 2e9, med;

  while (lo <= hi){
    med = (lo+hi)/2LL;
    if (med*(med+1) <= 2LL*dif){
      lo = med+1;
      res = med;
    } else {
      hi = med-1;
    }
  }

  return res;
}

ll bb2(ll i, ll R){
  ll res;
  ll lo = 0, hi = sqrt(R), med;

  while (lo <= hi){
    med = (lo+hi)/2LL;
    if (med*i + med*med > R){
      hi = med-1;
    } else {
      res = med;
      lo = med+1;
    }
  }

  return res;
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r", stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Test; cin >> Test;
  for (int caso=1; caso <= Test; caso++){
    cout << "Case #" << caso << ": ";
    ///
    /// acuerdate de limpiar las variables :)
    /// y de chequear los rangos :)
    ///
    cin >> L >> R;

    ll dif = abs(L-R);

    ll i = bb1(dif);

    if (L >= R){
      L -= i*(i+1)/2;
    } else {
      R -= i*(i+1)/2;
    }

    ll cant=0;
    if (R > L){
      ll k = bb2(i,R);

      R -= k*i + k*k;
      if (k > 0){
        L -= (k-1)*(i+k);

        cant = 2*k-1;
        if (L >= 2*k+i){
          L -= i+k*2;
          cant++;
        }
      }

      cout << i+cant << " " << L << " " << R << "\n";
    } else {
      ll k = bb2(i,L);
      L -= k*i + k*k;
      if (k > 0){
        R -= (k-1)*(i+k);

        cant = 2*k-1;
        if (R >= 2*k+i){
          R -= i+2*k;
          cant++;
        }
      }

      cout << i+cant << " " << L << " " << R << "\n";
    }
  }

  return 0;
}
