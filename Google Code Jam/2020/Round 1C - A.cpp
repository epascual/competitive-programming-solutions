#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> ii;

const int MN = 3e5+10;
const ll INF = 1e9;

char PATH[MN];

bool can_get(int X, int Y, int pasos){
  if (abs(X) + abs(Y) <= pasos) return true;
  else return false;
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  //freopen("a.out","w",stdout);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int TEST; cin >> TEST; for (int caso=1; caso <= TEST; caso++){
    cout << "Case #" << caso << ": ";

    int X, Y, N;
    cin >> X >> Y >> PATH;
    N = strlen(PATH);

    bool sepuede = false;
    for (int i=0; i < N; i++){
      if (PATH[i] == 'N') Y++;
      else if (PATH[i] == 'S') Y--;
      else if (PATH[i] == 'W') X--;
      else if (PATH[i] == 'E') X++;

      if (can_get(X,Y,i+1)){
        sepuede = true;
        cout << i+1 << "\n";
        break;
      }
    }
    if (!sepuede){
      cout << "IMPOSSIBLE\n";
    }
  }

  return 0;
}
