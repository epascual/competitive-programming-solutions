#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int,int> ii;

const int MN = 110;

string cad;

void program(){
  cin >> cad;
  int prev=0;
  for (int i=0; cad[i]; i++){
    int elem = cad[i]-'0';
    while (prev < elem){
      cout << '(';
      prev++;
    }
    while (prev > elem){
      cout << ')';
      prev--;
    }
    cout << cad[i];
  }
  while (prev > 0) {
    cout << ')';
    prev--;
  }
  cout << "\n";
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int casos; cin >> casos;
  for (int caso=1; caso <= casos; caso++){
    cout << "Case #" << caso << ": ";
    program();
  }

  return 0;
}
