#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;
const int MN = 1e5+10;

char cad[MN][110];
int len[MN];

vi toBin(int n){
  vi ret;
  while (n){
    ret.push_back(n%2);
    n /= 2;
  }
  //reverse(ret.begin(), ret.end());
  return ret;
}

void solve(vi &bin, int rest){
  //for (int x : bin) cout << x;
  //cout << "\n";
  bool inLeft = true;

  for (int i=0; i < bin.size(); i++){
    int x = i+1;
    if (inLeft){
      if (bin[i] == 1){
        inLeft = false;
        for (int j=1; j <= x; j++){
          cout << x << " " << j << "\n";
        }
      } else {
        cout << x << " " << 1 << "\n";
      }
    }
    else {
      if (bin[i] == 1){
        inLeft = true;
        for (int j=x; j >= 1; j--){
          cout << x << " " << j << "\n";
        }
      } else {
        cout << x << " " << x << "\n";
      }
    }
  }

  for (int i=1; i <= rest; i++){
    if (inLeft){
      cout << bin.size() + i << " " << 1 << "\n";
    } else {
      cout << bin.size() + i << " " << bin.size() + i << "\n";
    }
  }

}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  cin.tie(0); ios_base::sync_with_stdio(0);
  #endif // PRIVADO

  int Tests; cin >> Tests; for (int Test = 1; Test <= Tests; Test++){
    cout << "Case #" << Test << ":\n";
    int N; cin >> N;

    bool solved = false;
    int sobran = 0;
    while (!solved){
      vector<int> bin = toBin(N-sobran);

      int c0 = 0; for (int x : bin) if (!x) c0++;

      if (c0 <= sobran){
        solved = true;
        solve(bin, sobran - c0);
      }

      sobran++;
    }
  }

  return 0;
}
