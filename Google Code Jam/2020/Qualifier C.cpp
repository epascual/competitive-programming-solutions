#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int,int> ii;

const int MN = 1100;

struct Act {
  int ini, fin, idx;
  bool operator< (const Act &o) const{
    return (ini < o.ini) || (ini==o.ini && fin < o.fin);
  }
};

char cad[MN];
void program(){
  int N; cin >> N;
  vector<Act> act(N);
  for (int i=0; i < N; i++){
    cin >> act[i].ini >> act[i].fin;
    act[i].idx=i;
  }
  sort(&act[0], &act[N]);

  bool sepuede = true;
  cad[N] = 0;
  int C=-1, J=-1;
  for (int i=0; i < N; i++){
    char x;
    if (C == -1 || act[C].fin <= act[i].ini){
      C = i;
      x = 'C';
    } else if (J == -1 || act[J].fin <= act[i].ini){
      J = i;
      x = 'J';
    } else {
      sepuede = false;
      break;
    }

    cad[act[i].idx] = x;
  }
  if (!sepuede) cout << "IMPOSSIBLE\n";
  else cout << cad << "\n";
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int casos; cin >> casos;
  for (int caso=1; caso <= casos; caso++){
    cout << "Case #" << caso << ": ";
    program();
  }

  return 0;
}
