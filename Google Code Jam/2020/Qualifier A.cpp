#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<int,int> ii;

const int MN = 110;

int A[110][110];

void program(){
  int N; cin >> N;
  set<int> row[MN], col[MN];
  int trace = 0;
  for (int i=1; i <= N; i++){
    for (int j=1; j <= N; j++){
      cin >> A[i][j];
      row[i].insert(A[i][j]);
      col[j].insert(A[i][j]);
    }
    trace += A[i][i];
  }
  int r=0, c=0;
  for (int i=1; i <= N; i++){
    if (row[i].size() != N)
      r++;
    if (col[i].size() != N)
      c++;
  }
  cout << trace << " " << r << " " << c << "\n";
}

int main()
{
  #ifdef PRIVADO
  freopen("a.in","r",stdin);
  #else
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  #endif // acm

  int casos; cin >> casos;
  for (int caso=1; caso <= casos; caso++){
    cout << "Case #" << caso << ": ";
    program();
  }

  return 0;
}
